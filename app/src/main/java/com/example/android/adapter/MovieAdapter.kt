package com.example.android.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.android.R
import com.example.android.models.Movie
import com.squareup.picasso.Picasso


class MovieAdapter(private val context: Context, private val movieList: List<Movie>) : BaseAdapter() {

    private val inflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return movieList.size
    }

    override fun getItem(position: Int): Movie {
        return movieList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val rowView = inflater.inflate(R.layout.movies_list_item, parent, false)

        val titleTextView = rowView.findViewById(R.id.title) as TextView
        val yearTextView = rowView.findViewById(R.id.year) as TextView
        val posterImageView = rowView.findViewById(R.id.poster) as ImageView

        val movie = getItem(position) as Movie

        titleTextView.text = movie.title
        yearTextView.text = movie.year

        Picasso.with(context).load(movie.poster).placeholder(R.mipmap.ic_launcher).into(posterImageView)

        return rowView
    }
}