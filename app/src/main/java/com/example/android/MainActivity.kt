package com.example.android

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.android.adapter.MovieAdapter
import com.example.android.models.ListMovies
import com.example.android.models.Movie
import com.example.android.services.FirebaseService
import com.example.android.services.MovieService
import com.example.android.services.RetrofitClientInstance.retrofitInstance
import com.example.android.services.SqLiteService
import com.example.android.utils.Utils.Companion.convertToMovies
import com.example.android.utils.Utils.Companion.log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.GenericTypeIndicator
import com.google.firebase.database.ValueEventListener
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {

    private lateinit var listView: ListView
    private val sqLite = SqLiteService(this, null)
    private val firebase = FirebaseService()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val editText = findViewById<EditText>(R.id.movie_name)
        val showButton = findViewById<Button>(R.id.showInput)

        showButton.setOnClickListener {
            val text = editText.text
            getMoviesByNameFromCache(text.toString())
        }
    }

    private fun getMoviesByNameFromCache(name: String) {
        val moviesFromFCache = sqLite.getMoviesByCategory(name)
        if (moviesFromFCache != null && moviesFromFCache.moveToFirst()) {
            log("Movies retrieved from SQLite database")
            showMovies(convertToMovies(moviesFromFCache))
            return
        }
        val moviesFromSCache = firebase.getReference()
        moviesFromSCache.child(name).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (!snapshot.exists()) {
                    getMoviesByNameFromApi(name)
                    return
                }
                log("Movies retrieved from Firebase")
                val gti = object : GenericTypeIndicator<List<Movie>>() {}
                showMovies(snapshot.getValue(gti) as ArrayList<Movie>)
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        })
    }

    private fun getMoviesByNameFromApi(name: String) {
        val service = retrofitInstance!!.create(MovieService::class.java)
        val secondCall: Call<ListMovies?>? = service.getMoviesByName(name)
        secondCall?.enqueue(object : Callback<ListMovies?> {
            override fun onResponse(
                call: Call<ListMovies?>,
                response: Response<ListMovies?>,
            ) {
                val res = response.body()!!

                for (item in res.search!!) {
                    sqLite.addMovie(name.lowercase(), item!!)
                }
                val reference = firebase.getReferenceByName(name.lowercase())
                firebase.addMovieToReference(baseContext, reference, res.search)

                log("Movies retrieved from API")
                showMovies(res.search as List<Movie>)
            }

            override fun onFailure(call: Call<ListMovies?>, t: Throwable) {
                Toast.makeText(
                    this@MainActivity,
                    "Something went wrong...Please try later!",
                    Toast.LENGTH_SHORT
                ).show()
            }
        })
    }

    private fun showMovies(movies: List<Movie>) {
        listView = findViewById(R.id.movie_list_view)
        val adapter = MovieAdapter(baseContext, movies)
        listView.adapter = adapter
    }

}