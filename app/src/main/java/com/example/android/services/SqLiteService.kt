package com.example.android.services

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.android.models.Movie

class SqLiteService(context: Context, factory: SQLiteDatabase.CursorFactory?) :
    SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {
        val query = ("CREATE TABLE " + TABLE_NAME + " ("
                + TITLE + " TEXT PRIMARY KEY," +
                CATEGORY + " TEXT," +
                POSTER + " TEXT," +
                IMDBID + " TEXT," +
                YEAR + " TEXT" + ")")

        db.execSQL(query)
    }

    override fun onUpgrade(db: SQLiteDatabase, p1: Int, p2: Int) {
        // this method is to check if table already exists
        db.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db)
    }

    fun addMovie(category: String, movie : Movie){
        val values = ContentValues()

        values.put(CATEGORY, category)
        values.put(TITLE, movie.title)
        values.put(YEAR, movie.year)
        values.put(POSTER, movie.poster)
        values.put(IMDBID, movie.imdbID)

        val db = this.writableDatabase

        db.insertWithOnConflict(TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE)

        db.close()
    }


    fun getMoviesByCategory(category: String): Cursor? {

        val db = this.readableDatabase

        return db.rawQuery("SELECT * FROM $TABLE_NAME WHERE category=\"$category\"", null)
    }

    companion object{
        private const val DATABASE_NAME = "TEST1"
        private const val DATABASE_VERSION = 1
        const val TABLE_NAME = "test_table"
        const val CATEGORY = "category"
        const val TITLE = "title"
        const val POSTER = "poster"
        const val YEAR = "year"
        const val IMDBID = "imdbID"
    }
}