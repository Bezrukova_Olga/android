package com.example.android.services

import com.example.android.models.ListMovies
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieService {
    @GET("api")
    fun getMoviesByName(@Query(value="s") s: String): Call<ListMovies?>?
}