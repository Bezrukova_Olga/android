package com.example.android.services

import android.content.Context
import android.widget.Toast
import com.example.android.models.Movie
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class FirebaseService {

    private val database = Firebase.database

    fun getReference(): DatabaseReference {
        return database.reference
    }

    fun getReferenceByName(referenceName: String): DatabaseReference {
        return database.getReference(referenceName.toLowerCase())
    }

    fun addMovieToReference(ctx: Context, reference: DatabaseReference, movies: List<Movie?>?) {
        reference.setValue(movies)
            .addOnSuccessListener {
                Toast.makeText(
                    ctx,
                    "Movies was added successfully",
                    Toast.LENGTH_SHORT
                ).show()
            }
    }
}