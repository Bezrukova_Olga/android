package com.example.android.models

import com.google.gson.annotations.SerializedName

class ListMovies {
    @SerializedName("Search")
    val search: List<Movie?>? = null
}