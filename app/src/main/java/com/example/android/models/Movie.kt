package com.example.android.models

import com.google.gson.annotations.SerializedName

class Movie {
    @SerializedName("imdbID")
    var imdbID: String? = null
    @SerializedName("Title")
    var title: String? = null
    @SerializedName("Year")
    var year: String? = null
    @SerializedName("Poster")
    var poster: String? = null
}