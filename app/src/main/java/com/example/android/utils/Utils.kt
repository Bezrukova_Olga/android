package com.example.android.utils

import android.database.Cursor
import com.example.android.models.Movie

class Utils {
    companion object {
        fun convertToMovies(cursor: Cursor): ArrayList<Movie> {
            val movies: ArrayList<Movie> = ArrayList()
            cursor.moveToFirst()
            while (!(cursor.isAfterLast)) {
                val movie = Movie()
                movie.title = cursor.getString(cursor.getColumnIndexOrThrow("title"))
                movie.poster = cursor.getString(cursor.getColumnIndexOrThrow("poster"))
                movie.year = cursor.getString(cursor.getColumnIndexOrThrow("year"))
                movie.imdbID = cursor.getString(cursor.getColumnIndexOrThrow("imdbID"))
                movies.add(movie)
                cursor.moveToNext()
            }
            return movies
        }

        fun log(message: String) {
            println(message)
        }
    }
}