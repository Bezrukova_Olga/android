# Getting movies via API
The application allows you to search for a list of all movies that contain the value entered by the user and display information about them on the screen

## Functionality
* searching movies by name
* displaying movies

## Architecture
### High level architecture
![Android App Schema](docs/images/android_app_schema.png)
### Operation flow
![Logic](docs/images/logic.jpg)

## Prerequisites

* Already installed *Android Studio* (https://developer.android.com/studio) and *Node.js* (https://nodejs.org/en/download)

## Installation

* Clone repository containing data to display
```bash
git clone https://github.com/gautemo/fake-movie-database-api
```
* Go to the fake-movie-database-api directory and open the console
* Execute the `npm install` and `npm start` commands
* Clone repository and open in *Android Studio*
```bash
git clone https://gitlab.com/Bezrukova_Olga/android.git
```
* Start the application

## Dependencies

| Dependence | Link                                                          |
|------------|---------------------------------------------------------------|
| Retrofit   | https://github.com/square/retrofit?ysclid=lezr6gm1wv430080988 |
| Firebase   | https://firebase.google.com |
| SQLite     | https://www.sqlite.org/index.html |

## Databases structure

The database is used to store all information about films(name, year and poster).

## Global Reviewers & Maintainers
1. Bezrukova Olga (katlinc@mail.ru)
2. Galizina Elizaveta (liza.galizina@yandex.ru)
